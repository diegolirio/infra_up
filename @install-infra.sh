#!/bin/bash

config_yml="config.docker-compose.yml"
discovery_yml="discovery.docker-compose.yml"
admin_yml="admin.docker-compose.yml"
gateway_yml="gateway.docker-compose.yml"
product_yml="product.docker-compose.yml"

#docker network create -d bridge logistic-network

if [ ! -f $config_yml ]
then
    wget "https://bitbucket.org/diegolirio/infra_up/raw/94b6b176e157d389528a1fbb8a3e7f5c119bafbd/$config_yml"
fi

if [ ! -f $discovery_yml ]
then
    wget "https://bitbucket.org/diegolirio/infra_up/raw/94b6b176e157d389528a1fbb8a3e7f5c119bafbd/$discovery_yml"
fi

if [ ! -f $admin_yml ]
then
    wget "https://bitbucket.org/diegolirio/infra_up/raw/94b6b176e157d389528a1fbb8a3e7f5c119bafbd/$admin_yml"
fi

if [ ! -f $gateway_yml ]
then
    wget "https://bitbucket.org/diegolirio/infra_up/raw/94b6b176e157d389528a1fbb8a3e7f5c119bafbd/$gateway_yml"
fi

if [ ! -f $product_yml ]
then
    wget "https://bitbucket.org/diegolirio/infra_up/raw/94b6b176e157d389528a1fbb8a3e7f5c119bafbd/$product_yml"
fi

docker-compose -f $config_yml down
docker-compose -f $discovery_yml down
docker-compose -f $admin_yml down
docker-compose -f $gateway_yml down
docker-compose -f $product_yml down

docker-compose -f $config_yml up -d
docker-compose -f $discovery_yml up -d
docker-compose -f $admin_yml up -d
docker-compose -f $gateway_yml up -d
docker-compose -f $product_yml up -d


###### mongodb #####
#docker run --name logistic-mongodb -d mongo --auth
#docker exec -it logistic-mongodb mongo admin